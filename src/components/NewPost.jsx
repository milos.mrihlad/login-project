import React, { useState } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { withRouter } from "react-router";


function NewPost({history, token}) {
    const [title, setTitle] = useState('');
    const [text, setText] = useState('');


    const novyClanek = (event) => {
        event.preventDefault();
        event.stopPropagation();
        if (validace()) {
            axios.post('https://rest-paskal.herokuapp.com/api/v1/protected/add-post', {
                title: title,
                text: text
            },{
                headers: {
                    Authorization: `Bearer ${token}`
                }
            }).then(res => {
                toast.success(res.data.message, {
                    position: "top-center",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true
                });
                history.push('/posts');
            })
        }
    }

    const validace = () => {
        if (!title || !text) {
            toast.error('Nadpis a text je povinný !', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true
            });
            return false;
        }
        return true;
    }


    return (
        <form onSubmit={novyClanek}>
            <label>Nadpis:</label>
            <input onChange={(n) => setTitle(n.target.value)} value={title} type='text' />
            <label>Text</label>
            <textarea onChange={(n) => setText(n.target.value)} value={text} />
            <button type='submit'>Odeslat</button>
        </form>
    )
}

export default withRouter(NewPost)
