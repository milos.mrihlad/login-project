import React, { useState, useEffect } from 'react'
import axios from 'axios';
import {Link } from 'react-router-dom';

function Home({ token }) {
    const [clanky, setClanky] = useState([]);

    const nactiClanky = () => {
        axios.get('https://rest-paskal.herokuapp.com/api/v1/protected/posts', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then(res => {
            setClanky(res.data.posts);
        })
    }

    
    useEffect(() => {
        nactiClanky();
    }, [])

    const vykresliClanky = () => {
        return clanky.map((clanek, index) => {
            return (
                <div key={clanek.id}>
                    <p>{clanek.title}</p>
                    <p>{clanek.text}</p>
                    {index !== clanky.length - 1 ? <hr /> : null}
                </div>
            )
        }
        );
    }

    if (!token) {
        return null;
    }

    return (
        <div>
            <Link to='/posts/new'>Přidat článek</Link>
            {vykresliClanky()}
        </div>
    )
}

export default Home
