import React, { useState } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';

import { isEmailValid } from '../util/validator';

function Register() {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirm, setPasswordConfirm] = useState('');

    const validateForm = () => {
        const errors = [];
        if (password.length < 5) {
            errors.push('Heslo musí mít minimálně 5 znaků!');
        }
        if(!isEmailValid(email)){
            errors.push('Email není validní!');
        }
        if (!passwordConfirm || (password !== passwordConfirm)){
            errors.push('Hesla se neshodují!');
        }
        if(errors.length > 0){
            const errorResult = (
                <div>
                    {errors.map((message, index) => <p key={index}>{message}</p>)}
                </div>
            )
            toast.error(errorResult, {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true
            });
            return false;
        }
        return true;
    }

    const register = (event) => {
        event.preventDefault();
        event.stopPropagation();

        if (validateForm()) {
            axios.post('https://rest-paskal.herokuapp.com/api/v1/register', {
                username: email,
                password: password
            }).then(res => {
                setPassword('');
                setPasswordConfirm('');
                setEmail('');
                if (res && res.data) {
                    toast.success(res.data.message, {
                        position: "top-center",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }
            }).catch(err => console.error(err))
        }
    }


    return (
        <form noValidate onSubmit={register}>
            <label>Email:</label>
            <input value={email} onChange={e => setEmail(e.target.value)}type='email' />
            <label>Heslo:</label>
            <input value={password} onChange={e => setPassword(e.target.value)} type='password' />
            <label>Znova heslo:</label>
            <input value={passwordConfirm} onChange={e => setPasswordConfirm(e.target.value)} type='password' />
            <button type='submit'>Registrovat</button>
        </form>
    );

}

export default Register;
