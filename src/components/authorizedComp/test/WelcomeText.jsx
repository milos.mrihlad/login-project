import React, { useState, useEffect } from 'react'
import axios from 'axios';


function WelcomeText(props) {
    const [uzivat, setUzivat] = useState(null);

    useEffect(() => {
       setUzivat(null)
    }, [props.token])

    const nactiUzivatele = () => {
        axios.get('https://rest-paskal.herokuapp.com/api/v1/protected/who', {
            headers: {
                Authorization: `Bearer ${props.token}`
            }
        }).then(res => {
            setUzivat(res.data.username)
        })
    }

    const vykresliUzivatele = () => {
        if(!uzivat){
            return null;
        }
        
        return <h1> Vítej {uzivat} </h1>;
    }

    if (!props.token) {
        return null;
    }

    return (
        <div>
            <button onClick={nactiUzivatele}>Pozdrav Uživatele</button>
            {vykresliUzivatele()}
        </div>
    )
}

export default WelcomeText
