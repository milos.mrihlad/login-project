import React, { useState, useEffect } from 'react';
import axios from 'axios';

const Obrazek = ({ token }) => {
    const [src, setSrc] = useState(null);

    useEffect(() => {
        setSrc(null);
    }, [token])

    const getSrc = () => {
        axios.get('https://rest-paskal.herokuapp.com/api/v1/protected', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then(res => {
            setSrc(res.data.imgSrc)
        })
    }

    const renderSrc = () => {
        if (src) {
            return <img alt='Testovací obrázek' src={src} />;
        }
        return null;
    }

    if (!token) {
        return null;
    }

    return (
        <div>
            <button onClick={getSrc}>Načti obrázek</button>
            {renderSrc()}
        </div>
    )

};

export default Obrazek;
