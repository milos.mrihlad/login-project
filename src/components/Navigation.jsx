import { NavLink } from "react-router-dom";
import styled from "styled-components";
import { toast } from 'react-toastify';
import { withRouter } from "react-router";

const Nav = styled.div`
    margin-bottom: 0.5em;
    display: flex;
    background-color: #333;
    padding: 0 2em;

`;
const Row = styled.div`
    flex-direction: row;
`;
const Logo = styled.div`
    padding: 0.5em;
    color: White;
    font-size: 1.5em;
`;

const LinkStyles = `
    flex-direction: row;
    justify-content: flex-end;
    color: white;
    padding: 1em 2em;
    text-align:center;
    font-weight: bold;

        &:hover{
        text-decoration: none;
        background-color: #414141 ;
        color: white;
        font-weight: bold;
        }
        &:active{
        background-color:#04AA6D ;
        }
`;

const MujNavLink = styled(NavLink)`
    ${LinkStyles}
`;

const MujNavBtn = styled.span`
    ${LinkStyles}
    cursor: pointer;
`;

const Obal = styled.div`
    width: 960px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin: auto;
    flex-wrap: wrap;
`;

const Navigation = ({token, setToken, history}) => {

    const odhlasit = () => {
        setToken(null);
        toast.success('Byl jsi úspěšně odhlášen', {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true
        });
        history.push('/login');
    }

    return (
        <Nav>
            <Obal>
                <Logo>
                    Logo
                </Logo>
                <Row>
                    <MujNavLink to='/'>Uvodní stránka</MujNavLink>
                    {!token && <MujNavLink to='/register'>Registrace</MujNavLink>}
                    {!token && <MujNavLink to='/login'>Přihlášení</MujNavLink>}
                    {token && <MujNavLink to='/posts'>Články</MujNavLink> }
                    {token && <MujNavBtn onClick={odhlasit}>Odhlásit</MujNavBtn>}
                </Row>
            </Obal>
        </Nav>
    );
};


export default withRouter(Navigation);
