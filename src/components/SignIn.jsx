import React, { useState } from 'react';
import { Redirect } from 'react-router-dom'
import axios from 'axios';
import { toast } from 'react-toastify';

import { isEmailValid } from '../util/validator';

function SignIn({token, setToken}) {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    //headers: {"Authorization" : `Bearer ${token}`,

    const handlePrihlaseni = (event) => {
        event.preventDefault();

        if (validateForm()) {
            axios.post('https://rest-paskal.herokuapp.com/api/v1/login', {
                username: email,
                password: password
            }).then(res => {
                if (res.data._token) {
                    setToken(res.data._token);
                    toast.success(res.data.message, {
                        position: "top-center",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true
                    });
                    setPassword('');
                    setEmail('');
                } else {
                    toast.error(res.data.message, {
                        position: "top-center",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true
                    });
                    setPassword('');
                }
            }).catch(err => console.error(err))
        }
    }

    const validateForm = () => {
        const errors = [];

        if (!isEmailValid(email)) {
            errors.push('Email není validní');
        }
        if (!password) {
            errors.push('Heslo je povinné')
        }

        if (errors.length > 0) {
            const errorResult = (
                <div>
                    {errors.map((message, index) => <p key={index}>{message}</p>)}
                </div>
            )
            toast.error(errorResult, {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true
            });
            return false;
        }
        return true;
    }

    if(token){
        return <Redirect to='/' />
    }

    return (
        <>
            <form noValidate onSubmit={handlePrihlaseni}>
                <label>Email: </label>
                <input onChange={e => setEmail(e.target.value)} value={email} type='email'/>
                <label>Heslo: </label>
                <input onChange={e => setPassword(e.target.value)} value={password} type='password' />
                <button type='submit'>Přihlásit se</button>
            </form>
        </>
    )
}

export default SignIn;
