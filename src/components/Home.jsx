import React from 'react';
import Obrazek from './authorizedComp/test/Obrazek';
import WelcomeText from './authorizedComp/test/WelcomeText';

const Home = ({token}) => {
    return (
        <>
            <div>Vitej doma</div>
            <Obrazek token={token} />
            <WelcomeText token={token}/>
        </>
    )
}

export default Home;