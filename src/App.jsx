import React, {useState} from 'react'
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import SignIn from './components/SignIn';
import Register from './components/Register';
import Navigation from './components/Navigation';
import Posts from './components/Posts';
import NewPost from './components/NewPost';
import Home from './components/Home';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  width: 95vh;
  margin: auto;
`;

function App() {
  const [token, setToken] = useState(null);

  return (
    <BrowserRouter>
      <ToastContainer />
      <Navigation token={token} setToken={setToken} />
      <Container>
        <Switch>
          <Route exact path='/'>
            <Home token={token}/>
          </Route>
          <Route exact path='/posts'>
            <Posts token={token}/>
          </Route>
          <Route exact path='/posts/new'>
            <NewPost token={token}/>
          </Route>
          <Route exact path='/login'>
            <SignIn token={token} setToken={setToken}/>
          </Route>
          <Route exact path='/register'>
            <Register />
          </Route>
        </Switch>
      </Container>
    </BrowserRouter>

  );
}

export default App;
